package app.barracuda.mail;

import android.app.*;
import android.os.*;
import android.webkit.*;

public class MainActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		WebView wv = (WebView) findViewById(R.id.wv);
		wv.setWebViewClient(new WebViewClient());
		wv.getSettings().setJavaScriptEnabled(true);
		wv.getSettings().setAppCacheEnabled(true);
		wv.loadUrl("https://valkyrie.barracudaprivacy.com");
		
    }
}
